################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../TivaTest.cpp 

CMD_SRCS += \
../tm4c123gh6pm.cmd 

C_SRCS += \
../tm4c123gh6pm_startup_ccs.c 

C_DEPS += \
./tm4c123gh6pm_startup_ccs.d 

OBJS += \
./TivaTest.obj \
./tm4c123gh6pm_startup_ccs.obj 

CPP_DEPS += \
./TivaTest.d 

OBJS__QUOTED += \
"TivaTest.obj" \
"tm4c123gh6pm_startup_ccs.obj" 

C_DEPS__QUOTED += \
"tm4c123gh6pm_startup_ccs.d" 

CPP_DEPS__QUOTED += \
"TivaTest.d" 

CPP_SRCS__QUOTED += \
"../TivaTest.cpp" 

C_SRCS__QUOTED += \
"../tm4c123gh6pm_startup_ccs.c" 


